// using bootstrap 5 and jquery

// let modal = new CustomModal('custom_id', 'modal_container_id', '');
// modal.setDescription('آیا از حذف این فایل مطمئن هستید؟')
//     .setModalType(modal.id)
//     .addButton('حذف', "goToUrl('/')" , 'btn btn-danger w-100', '', true)
//     .addButton('انصراف', "goToUrl('/')" , 'btn btn-outline-secondary w-100', '', true)
//     .setModalType(modal.MODAL_TYPE_QUESTION_DELETE)
//     .show();

class CustomModal {

    MODAL_TYPE_ALERT_SUCCESS = 'alert-success';
    MODAL_TYPE_ALERT_ERROR = 'alert-error';
    MODAL_TYPE_ALERT_INFO = 'alert-info';
    MODAL_TYPE_QUESTION = 'question';
    MODAL_TYPE_QUESTION_DELETE = 'question-delete';

    id = '';
    title = '';
    description = '';
    containerId = '';
    buttons = [];
    modalType = 'alert';

    constructor(id, containerId, title ='') {
        this.id = id;
        this.containerId = containerId;
        this.title = title;
    }

    setTitle(title) {
        this.title = title;
        return this;
    }

    setDescription(description) {
        this.description = description;
        return this;
    }

    setModalType(value) {
        this.modalType = value;
        return this;
    }

    addButton(title , action='' , classes, styles, closeDialog = true){
        let closeString = '';
        let onClick = '';
        if (closeDialog) {
            closeString = 'data-bs-dismiss="modal"'
        }

        this.buttons.push('<button type="button" class="' + classes + '" style="' + styles + '" ' + closeString + ' onclick="'+ action +'">' +
            title + '</button>');
        return this;
    }


    show() {
        let buttonsString = '';
        this.buttons.forEach(item => {
            buttonsString += item;
        });
        console.log(buttonsString);

        let icon = '';
        if (this.modalType === this.MODAL_TYPE_ALERT_SUCCESS) {
            icon = '<i class="fas fa-4x fa-check-circle mx-auto mt-4 mb-4" style="display: block; color: #198754"></i>';
        }

        if (this.modalType === this.MODAL_TYPE_ALERT_INFO) {
            icon = '<i class="fas fa-4x fa-info-circle mx-auto mt-4 mb-4" style="display: block; color: #007bff"></i>';
        }

        if (this.modalType === this.MODAL_TYPE_QUESTION) {
            icon = '<i class="fas fa-4x fa-question-circle mx-auto mt-4 mb-4" style="display: block; color: #007bff"></i>';
        }

        if (this.modalType === this.MODAL_TYPE_QUESTION_DELETE) {
            icon = '<i class="fas fa-4x fa-trash mx-auto mt-4 mb-4" style="display: block; color: #dc3545"></i>';
        }

        if (this.modalType === this.MODAL_TYPE_ALERT_ERROR) {
            icon = '<i class="fas fa-4x fa-info-circle mx-auto mt-4 mb-4" style="display: block; color: #dc3545"></i>';
        }

        let modal =
            '<div class="modal fade" id="' + this.id + '" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">';

        if (this.title != '') {
            modal =
                modal +
                '<div class = "modal-header">' +
                '<h5 class = "modal-title">' + this.title + '</h5>' +
                '</div>';
        }

        modal = modal +
            '<div class="modal-body text-center">' +
            icon +
            this.description +
            '<br>' +
            '</div>' +
            '<div class="modal-footer">' +
            buttonsString +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        $("#" + this.containerId).html(modal);

        let myModal = new bootstrap.Modal(document.getElementById(this.id), {
            keyboard: false
        })
        myModal.toggle()
    }


}

